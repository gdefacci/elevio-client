package com.github.gdefacci.service

import cats.effect.{Resource, Sync}
import org.http4s._
import cats.syntax.functor._
import cats.syntax.traverse._
import cats.syntax.flatMap._
import cats.instances.list._
import org.http4s.client.Client

class ElevioArticlesUrls(origin:Option[UriOrigin], pageSize:Int) {
  private val base = Uri(origin.map(_.scheme), origin.map(_.authority))

  def getArticles(page: Int): (Method, Uri) =
    Method.GET -> base
      .withPath("/v1/articles")
      .withQueryParam("page", page)
      .withQueryParam("page_size", pageSize)

  def getArticleById(articleId: Int): (Method, Uri) =
    Method.GET -> base
      .withPath(s"/v1/articles/$articleId")

}

object ElevioArticlesRepository {

  case class Configuration[F[_]](client: Resource[F, Client[F]],
                                 routes: ElevioArticlesUrls,
                                 apiKey: String,
                                 token: String)
}

class ElevioArticlesRepository[F[_]: Sync](
    configuration: ElevioArticlesRepository.Configuration[F])
    extends ArticlesRepository[F] {

  import org.http4s.circe.CirceEntityDecoder._
  import org.http4s._
  import configuration.routes

  private lazy val authHeaders = List(
    "x-api-key" -> configuration.apiKey,
    "Authorization" -> s"Bearer ${configuration.token}"
  )

  private def request(p: (Method, Uri)): Request[F] =
    Request[F](
      p._1,
      uri = p._2,
      headers = Headers.of(authHeaders.map {
        case (k, v) => Header(k, v)
      }: _*)
    )

  def fetch[T](req:Request[F])(implicit dec:EntityDecoder[F,T]):F[T] =
    configuration.client.use { client =>
      client.fetchAs[T](req)
    }

  def getArticles(page: Int): F[Page[JsonModel.Article]] =
    for {
      arts <- fetch[JsonModel.Articles](request(routes.getArticles(page)))
    } yield Page(arts.articles, arts.pageNumber, arts.totalPages)


  def getArticlesByKeyword(keyword: String,
                           page: Int): F[Page[JsonModel.Article]] = {
    for {
      arts <- fetch[JsonModel.Articles](request(routes.getArticles(1)))
      totPages = arts.totalPages
      remainingArticles <- 2.to(totPages).toList.traverse { pgn =>
        fetch[JsonModel.Articles](request(routes.getArticles(pgn))).map(_.articles)
      }
      all = arts.articles ++ remainingArticles.flatten
      res = all.filter( _.keywords.map(_.toLowerCase).contains(keyword.toLowerCase) )
    } yield Page.of(res.toList, page, arts.pageSize)
  }

  def getArticleById(id: Int): F[JsonModel.Article] =
    fetch[JsonModel.ArticleResult](request(routes.getArticleById(id))).map(_.article)


}
