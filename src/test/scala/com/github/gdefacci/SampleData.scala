package com.github.gdefacci
import com.github.gdefacci.service.JsonModel
import java.time.ZonedDateTime
import com.github.gdefacci.service.JsonModel.Translation

object SampleData {

  def article(idx:Int = 1) = JsonModel.Article(
    idx, 
    idx, 
    Some("Title"), 
    Some(JsonModel.Author(
      idx,
      s"user $idx",
      s"http://gravatar.com/$idx",
      "user-1@mail.it"
    )),
    "source",
    None,
    "editor version",
    Some("Notes"),
    List("keyword-1", "keyword-2"),
    1,
    "access",
    Nil, Nil, None, Nil,
    "published",
    None, None, None, None,
    ZonedDateTime.now(),
    List(Translation(
      "en",
      Some("the title"),
      Some("Body"),
      None, None
    ))
  )

  def articles(count:Int):Seq[JsonModel.Article] = 
    1.to(count).map(article)

}