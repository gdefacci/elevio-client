package com.github.gdefacci.web

import cats.effect._
import org.http4s.server.blaze.BlazeServerBuilder
import com.github.gdefacci.service.{ArticlesRepository, ElevioArticlesRepository, ElevioArticlesUrls, UriOrigin}
import org.http4s.server.Router
import org.http4s.implicits._
import org.http4s._

import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors

import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder

class HTTPApp[F[_] : Sync : ContextShift](
               appUrls: AppUrls,
               articleRepository: ArticlesRepository[F]
             ) {

  val blockingExecutionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  private val templates = new Templates(appUrls)
  private val routes = new Routes[F]

  val router = Router(
    "/articles" -> routes.htmlArticles(articleRepository, templates),
    "/" -> routes.home(templates, blockingExecutionContext)
  ).orNotFound

}

object Main extends IOApp {

  private def articleRepository[F[_] : ConcurrentEffect](key:String, token:String, pageSize:Int): ArticlesRepository[F] = {

    def httpClientResource:Resource[F, Client[F]] =
      BlazeClientBuilder[F](scala.concurrent.ExecutionContext.Implicits.global).resource

    val elevioEndpointOrigin = UriOrigin(
      Uri.Scheme.https,
      Uri.Authority(None, Uri.RegName("api.elevio-staging.com"), None))

    new ElevioArticlesRepository[F](ElevioArticlesRepository.Configuration[F](
      httpClientResource,
      new ElevioArticlesUrls(
        Some(elevioEndpointOrigin),
        pageSize),
      key,
      token
    ))
  }

  def run(args: List[String]): IO[ExitCode] = {

    CommandLineOptions.startServerCommand.parse(args, Map.empty) match {
      case Left(help) =>
        IO {
          println(help.toString)
        } map (_ => ExitCode.Success)

      case Right(commandLineOpts) =>
        val host = "localhost"
        val port = commandLineOpts.port

        if (port < 1024 || port > 65535) IO.raiseError(new RuntimeException(s"Invalid port number $port (must be > 1023 and <= 65535)"))
        else if (commandLineOpts.pageSize < 0) IO.raiseError(new RuntimeException(s"Invalid page size $port (must be > 0)"))
        else {
          val app = new HTTPApp[IO](
            new AppUrls(Some(UriOrigin(Uri.Scheme.http, Uri.Authority(None, Uri.RegName(host), Some(port))))),
            articleRepository[IO](commandLineOpts.key, commandLineOpts.token, commandLineOpts.pageSize))

          BlazeServerBuilder[IO]
            .bindHttp(port, host)
            .withHttpApp(app.router)
            .serve
            .compile
            .drain
            .map(_ => ExitCode.Success)
            .guarantee(IO(app.blockingExecutionContext.shutdown()))
        }

    }

  }
}