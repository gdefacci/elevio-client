package com.github.gdefacci

import cats.effect.IO
import org.http4s.{Uri, scalaxml}

import scala.io.Source

import cats.syntax.traverse._
import cats.instances.list._

object TestUtil {

  object Xml {

    implicit val xmlDecoder = scalaxml.xml[IO]

    implicit class TestXmlNode(node:scala.xml.Node) {

      def descendantsByAttribute(name:String, value:String):Seq[scala.xml.Node] =
        (node \\ "_").filter( nd => nd.attributes.exists( att => att.value.text == value && att.key == name) )

      def getElementsByClassName(className:String) =
        descendantsByAttribute("class", className)

      lazy val hrefs:IO[List[Uri]] = (node \\ "a").map { _.attribute("href").get.text }.toList.traverse { h =>
        IO.fromEither(Uri.fromString(h))
      }

      lazy val href =
        for {
          hrfs <- hrefs
          href <-
            if (hrfs.length == 1) IO.pure(hrfs.head)
            else IO.raiseError(new RuntimeException(s"expecting 1 link, got ${hrfs.length}"))
        } yield href

    }

  }


  def classpathRead(path: String): IO[String] =
    for {
      inputStream <- IO {
        Thread.currentThread.getContextClassLoader().getResourceAsStream(path)
      }
      cont <- IO {
        Source.fromInputStream(inputStream, "UTF-8").getLines.mkString("\n")
      }
    } yield cont

}