package com.github.gdefacci

import com.github.gdefacci.service.Page
import org.specs2._
import org.specs2.specification.core.Fragment

object PageSpec extends mutable.Specification {

  def checkPage[T](lst:Seq[T], pageSize:Int) =
    s"Page with ${lst.length} items and page size $pageSize" >> {
      def getPage(p:Int) = Page.of(lst, p, pageSize)

      assert(pageSize > 0)

      val numberOfPages = Page.numberOfPages(lst.length, pageSize)

      s"All pages, but last, must have $pageSize items" >> {
        Fragment.foreach(1 to (numberOfPages -1)) { pgn =>
          val page = getPage(pgn)
          s"page ${pgn.toString} has $pageSize items" ! {
            (page.items.length must_== pageSize) and
              (page.pageNumber must_== pgn) and
              (page.total must_== numberOfPages)
          }
        }
      }

      "Last page must have remaining items" >> {
        val page = getPage(numberOfPages)
        val numberOfItems =
          if (lst.length == 0) 0
          else {
            (lst.length % pageSize) match {
              case 0 => pageSize
              case x => x
            }
          }

        (page.items.length must_== numberOfItems) and
          (page.pageNumber must_== numberOfPages) and
          (page.total must_== numberOfPages)
      }
    }

  checkPage(1 to 20, 7)
  checkPage(Nil, 7)
  checkPage(1 to 4, 7)
  checkPage(1 to 4, 2)
  checkPage(1 to 20, 20)


}
