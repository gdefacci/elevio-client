package com.github.gdefacci.web

import cats.MonadError
import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import com.github.gdefacci.service.{ArticlesRepository, UriOrigin}
import cats.syntax.flatMap._
import cats.syntax.functor._
import org.http4s._
import org.http4s.headers._
import org.http4s.dsl.impl.QueryParamDecoderMatcher

import scala.concurrent.ExecutionContext
import cats.effect.ContextShift
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher

class AppUrls(origin:Option[UriOrigin]) {
  private val base = Uri(origin.map(_.scheme), origin.map(_.authority))

  def home = base

  def getArticles(page: Int): Uri =
    base
      .withPath("/articles")
      .withQueryParam("page", page)

  def submitKeywordSearch: Uri =
    base
      .withPath(s"/articles/search")

  def getArticleById(articleId: Int): Uri =
    base
      .withPath(s"/articles/${articleId}")

  def keywordSearch(keyword: String, page: Int): Uri =
    base
      .withPath("/articles/search")
      .withQueryParam("keyword", keyword)
      .withQueryParam("page", page)
}

class Routes[F[_] : Sync : ContextShift] {

  private object PageNumberParamMatcher
    extends OptionalQueryParamDecoderMatcher[Int]("page")

  private object KeywordParamMatcher
    extends QueryParamDecoderMatcher[String]("keyword")

  private val dsl = new Http4sDsl[F] {}

  import dsl._

  private def html(elem: xml.Node) =
    Ok(elem.toString)
      .map(_.withContentType(`Content-Type`(MediaType.text.`html`)))

  private def static(file: String,
                     request: Request[F],
                     blockingEc: ExecutionContext) =
    StaticFile
      .fromResource("/" + file, blockingEc, Some(request))
      .getOrElseF(NotFound())

  def attempt(templates: Templates, fx: F[xml.Node]): F[xml.Node] = for {
    vOrErr <- MonadError[F, Throwable].attempt(fx)
  } yield {
    vOrErr match {
      case Left(err) => templates.main(
        "Error",
        templates.errorPage(err.getMessage)
      )
      case Right(v) => v
    }
  }

  def htmlArticles(
                    articleRepository: ArticlesRepository[F],
                    templates: Templates
                  ) =
    HttpRoutes.of[F] {
      case GET -> Root :? PageNumberParamMatcher(pageNumber) =>
        for {
          cont <- attempt(templates, articleRepository.getArticles(pageNumber.getOrElse(1)).map { arts =>
            templates.main(
              "Articles",
              templates.articles(arts, templates.articlesNavigation(arts))
            )
          })
          resp <- html(cont)
        } yield resp

      case GET -> Root / "search" :? KeywordParamMatcher(keyword) +& PageNumberParamMatcher(
      pageNumber
      ) =>
        if (keyword.trim.isEmpty)
          html(
            templates.main("Home",
              templates.homepageContent(
                HomePageErrors(Some("keyword cant be empty")))))
        else
          for {
            cont <- attempt(templates, articleRepository.getArticlesByKeyword(
              keyword,
              pageNumber.getOrElse(1)
            ).map { arts =>
              templates.main(
                s"Articles containing '${keyword}'",
                templates
                  .articles(arts,
                    templates.keywordSearchNavigation(keyword, arts))
              )
            })

            resp <- html(cont)
          } yield resp

      case GET -> Root / IntVar(articleId) =>
        for {
          cont <- attempt(templates, articleRepository.getArticleById(articleId).map { article =>
            templates.main(
              s"Article ${article.title.getOrElse("no-title")}",
              templates.articleDetails(article)
            )
          })
          resp <- html(cont)
        } yield resp
    }

  def home(
            templates: Templates,
            blockingEc: ExecutionContext
          ) =
    HttpRoutes.of[F] {
      case GET -> Root =>
        html(templates.main("Home", templates.homepageContent()))
      case req@GET -> Root / "css" / "main.css" =>
        static("main.css", req, blockingEc)
    }

}
