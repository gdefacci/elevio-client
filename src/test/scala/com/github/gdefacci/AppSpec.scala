package com.github.gdefacci

import cats.effect.IO

import com.github.gdefacci.web.AppUrls
import com.github.gdefacci.service._
import org.http4s._

import org.specs2._
import org.specs2.matcher._

object AppSpec extends mutable.Specification with IOMatchers with ResultMatchers {
  sequential

  import TestUtil.Xml._

  private val app = {

    val articlesRepository = {
      def setKeywords: ((JsonModel.Article, Int)) => JsonModel.Article = {
        case ((p, idx)) =>
          val kws = (idx % 4) match {
            case 0 => List("the-keyword")
            case _ => Nil
          }
          p.copy(keywords = kws)
      }

      val testClient = TestClient(SampleData.articles(28).zipWithIndex.map(setKeywords))
      new ElevioArticlesRepository(new ElevioArticlesRepository.Configuration[IO](
        testClient,
        new ElevioArticlesUrls(None, 8),
        "key",
        "token"
      ))
    }

    val appUrls = new AppUrls(None)
    new com.github.gdefacci.web.HTTPApp(appUrls, articlesRepository)
  }

  private def expectArticles(pred: Seq[scala.xml.Node] => Boolean) = (nd:scala.xml.Node) =>
    IO { assert( pred( nd.getElementsByClassName( "article-item"))) }

  private def expectPageNumberIs(pageNumber: Int) = (nd:scala.xml.Node) =>
    IO { assert(nd.getElementsByClassName( "page-number").text == pageNumber.toString) }

  private def expectPageTotalIs(pageTotal: Int) = (nd:scala.xml.Node) =>
    IO { assert(nd.getElementsByClassName( "page-total").text == pageTotal.toString) }

  private def successfulGet(uri: Uri): IO[Response[IO]] = for {
    response <- app.router.run(
      Request(method = Method.GET, uri = uri)
    )
    _ <- IO {
      assert(response.status == Status.Ok)
    }
  } yield response

  private val successfulGetArticlesFirstPage: IO[Unit] = for {
    response <- successfulGet(uri"/articles?page=1")
    content <- response.as[scala.xml.Elem]
    _ <- expectArticles(_.length == 8)(content)
    _ <- expectPageNumberIs(1)(content)
    _ <- expectPageTotalIs(4)(content)
  } yield ()

  private val successfulGetArticlesSecondPage: IO[Unit] = for {
    response <- successfulGet(uri"/articles?page=2")
    content <- response.as[scala.xml.Elem]
    _ <- expectArticles(_.length == 8)(content)
    _ <- expectPageNumberIs(2)(content)
    _ <- expectPageTotalIs(4)(content)
  } yield ()

  private val successfulGetArticlesLastPage: IO[Unit] = for {
    response <- successfulGet(uri"/articles?page=4")
    content <- response.as[scala.xml.Elem]
    _ <- expectArticles(_.length == 4)(content)
  } yield ()

  private val successfulSearchArticlesFirstPage: IO[Unit] = for {
    response <- successfulGet(uri"/articles/search?keyword=the-keyword&page=1")
    content <- response.as[scala.xml.Elem]
    _ <- expectArticles(_.length == 7)(content)
    _ <- expectPageNumberIs(1)(content)
    _ <- expectPageTotalIs(1)(content)
  } yield ()

  private val searchArticlesInvalidPageDisplayFieldError: IO[Unit] = for {
    response <- successfulGet(uri"/articles/search?page=1&keyword=")
    content <- response.as[scala.xml.Elem]
    _ <- IO {
      assert(content.getElementsByClassName( "field-error").size == 1)
    }
  } yield ()

  private val getHomeExpectContainsGetArticlesAndSearchByKeyword = for {
    response <- successfulGet(uri"/")
    content <- response.as[scala.xml.Elem]
    _ <- IO {
      assert(content.getElementsByClassName( "get-articles").size == 1)
    }
    _ <- IO {
      assert(content.getElementsByClassName( "search-by-keyword").size == 1)
    }
  } yield ()

  val navigateToArticleDetail = for {
    response <- successfulGet(uri"/")
    content <- response.as[scala.xml.Elem]
    getArticlesSection <- IO {
      content.getElementsByClassName( "get-articles").head
    }
    getArticlesUri <- getArticlesSection.hrefs.map(_.head)
    responseGetArticles <- successfulGet(getArticlesUri)
    contentGetArticles <- responseGetArticles.as[scala.xml.Elem]
    firstArt <- IO {
      contentGetArticles.getElementsByClassName( "article-item").head
    }
    getArticleUri <- firstArt.href
    responseGetArticle <- successfulGet(getArticleUri)
    contentGetArticle <- responseGetArticle.as[scala.xml.Elem]
    _ <- IO {
      assert(contentGetArticle.getElementsByClassName( "article").size == 1)
    }
  } yield ()

  "Home" >> {
    "contains get-articles and search-by-keyword sections" >> {
      getHomeExpectContainsGetArticlesAndSearchByKeyword must returnOk
    }
  }
  "Get Articles" >> {
    "get page 1 is successful" >> {
      successfulGetArticlesFirstPage must returnOk
    }
    "get page 2 is successful" >> {
      successfulGetArticlesSecondPage must returnOk
    }
    "get last page is successful" >> {
      successfulGetArticlesLastPage must returnOk
    }
  }
  "Search articles by keyword" >> {
    "get page 1 is successfull" >> {
      successfulSearchArticlesFirstPage must returnOk
    }
    "on empty keyword a field error is displayed" >> {
      searchArticlesInvalidPageDisplayFieldError must returnOk
    }
  }
  "Article detail" >> {
    "can navigate to article details page" >> {
      navigateToArticleDetail must returnOk
    }
  }


}
