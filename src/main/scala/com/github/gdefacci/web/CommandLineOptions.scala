package com.github.gdefacci.web

import cats.syntax.apply._

case class CommandLineOptions(key:String, token:String, port:Int, pageSize:Int)

object CommandLineOptions {

  import com.monovore.decline._

  private val key = Opts.option[String]("key",
    short = "k",
    metavar = "key",
    help = "Set the key (x-api-key header).")

  private val token = Opts.option[String]("token",
    short = "t",
    metavar = "token",
    help = "Set the token (Authorization header).")

  private val portNumber = Opts.option[Int]("port",
    short = "p",
    metavar = "port",
    help = "Set the http server port.") withDefault 8080

  private val pageSize = Opts.option[Int]("page-size",
    short = "s",
    metavar = "page size",
    help = "Set page size.") withDefault 4

  def startServerCommand = Command(
    name = """sbt runMain com.github.gdefacci.web.Main""",
    header = "Start the Articles server"
  ) {
    (key, token, portNumber, pageSize).mapN(CommandLineOptions(_,_,_,_))
  }
}
