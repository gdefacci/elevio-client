package com.github.gdefacci.service

import org.http4s.Uri

case class UriOrigin(scheme: Uri.Scheme, authority: Uri.Authority)
