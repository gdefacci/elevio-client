package com.github.gdefacci

import cats.effect.IO
import com.github.gdefacci.service.{ElevioArticlesRepository, ElevioArticlesUrls}
import com.github.gdefacci.web.AppUrls
import org.http4s._
import org.specs2._
import org.specs2.matcher._

object FailingAppSpec extends mutable.Specification with IOMatchers with ResultMatchers  {
  sequential

  import TestUtil.Xml._

  private val app = {

    val articlesRepository = {
      val testClient = TestClient.const(Status.InternalServerError)
      new ElevioArticlesRepository(new ElevioArticlesRepository.Configuration[IO](
        testClient,
        new ElevioArticlesUrls(None, 8),
        "key",
        "token"
      ))
    }
    val appUrls = new AppUrls(None)
    new com.github.gdefacci.web.HTTPApp(appUrls, articlesRepository)
  }

  private def expectErrorPage = (nd:scala.xml.Node) =>
    IO { assert(nd.getElementsByClassName("error-page").length == 1) }

  private def successfulGet(uri:Uri):IO[Response[IO]] = for {
    response <- app.router.run(
      Request(method = Method.GET, uri = uri)
    )
    _ <- IO { assert(response.status == Status.Ok) }
  } yield response

  private val errorPageOnGetArticles:IO[Unit] = for {
    response <- successfulGet(uri"/articles?page=1")
    content <- response.as[scala.xml.Elem]
    _ <- expectErrorPage(content)
  } yield ()

  private val errorPageOnSearchArticles:IO[Unit] = for {
    response <- successfulGet(uri"/articles/search?keyword=keyword&page=1")
    content <- response.as[scala.xml.Elem]
    _ <- expectErrorPage(content)
  } yield ()

  private val getHome = for {
    response <- successfulGet(uri"/")
    content <- response.as[scala.xml.Elem]
    _ <- IO { assert(content.getElementsByClassName("get-articles").size == 1) }
    _ <- IO { assert(content.getElementsByClassName("search-by-keyword").size == 1) }
  } yield ()

  "get articles when ArticlesRepository fails" >> {
    "error page is displayed" >> {
      errorPageOnGetArticles must returnOk
    }
  }
  "search articles when ArticlesRepository fails" >> {
    "error page is displayed" >> {
      errorPageOnSearchArticles must returnOk
    }
  }
  "home when ArticlesRepository fails" >> {
    "is properly displayed" >> {
      getHome must returnOk
    }
  }

}
