package com.github.gdefacci

import cats.effect.{IO, Resource}
import com.github.gdefacci.service.{JsonModel, Page}
import org.http4s._
import org.http4s.circe._
import org.http4s.client.Client
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.QueryParamDecoderMatcher

object TestClient {

  private object PageNumberParamMatcher
      extends QueryParamDecoderMatcher[Int]("page")

  private object PageSizeParamMatcher
      extends QueryParamDecoderMatcher[Int]("page_size")

  private val dsl = new Http4sDsl[IO] {}

  import dsl._

  def const(status:Status): Resource[IO, Client[IO]] = Resource.pure {
    Client[IO]({

      case GET -> Root / "v1" / "articles" :?
        PageNumberParamMatcher(_) +&
          PageSizeParamMatcher(_) =>

        Resource.pure {
          Response(status)
        }

      case GET -> Root / "v1" / "articles" / _ =>

        Resource.pure {
          Response(status)
        }
    })
  }

  def apply(articles: Seq[JsonModel.Article]): Resource[IO, Client[IO]] = {
    val client = Client[IO]({

      case GET -> Root / "v1" / "articles" :? 
                    PageNumberParamMatcher(pageNumber) +& 
                    PageSizeParamMatcher(pageSize) =>

        val pg = Page.of(articles, pageNumber, pageSize)
        val arts = JsonModel.Articles(pg.items, pg.pageNumber, pageSize, pg.total, pg.items.length)

        Resource.pure { 
          Response(Status.Ok).withEntity(arts)(jsonEncoderOf[IO, JsonModel.Articles]) 
        }

      case GET -> Root / "v1" / "articles" / articleId =>

        Resource.pure {
          articles.find(art => art.id == articleId.toInt) match {
            case None =>
              Response(Status.NotFound)
            case Some(art) =>
              Response(Status.Ok).withEntity(JsonModel.ArticleResult(art))(
                jsonEncoderOf[IO, JsonModel.ArticleResult]
              )
          }
        }
    })

    Resource.pure { client }
  }

}
