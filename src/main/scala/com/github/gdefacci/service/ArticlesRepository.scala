package com.github.gdefacci.service

import JsonModel._

case class Page[T](items:Seq[T], pageNumber:Int, total:Int)

object Page {

  def of[T](items:Seq[T], page:Int, pgSize:Int):Page[T] = {
    val pageSize = if (pgSize < 1) 1 else pgSize
    val pageTotal = numberOfPages( items.length, pageSize)
    val pg =
      if (page < 1) 1
      else if (page > pageTotal) pageTotal
      else page

    val firstIdx = (pg - 1) * pageSize
    val lastIdx = firstIdx + pageSize

    Page(items
      .zipWithIndex
      .collect {
        case (at, idx) if (idx >= firstIdx) && (idx < lastIdx) => at
      }, pg, pageTotal)
  }

  def numberOfPages(itemsTotal:Int, pageSize:Int):Int =
    if (itemsTotal == 0) 1
    else if (pageSize < 1) itemsTotal
    else Math.ceil( itemsTotal.toFloat / pageSize).toInt

}

trait ArticlesRepository[F[_]] {

  def getArticles(page:Int):F[Page[Article]]
  def getArticlesByKeyword(keyword:String, page:Int):F[Page[Article]]
  def getArticleById(id:Int):F[Article]
  
}

