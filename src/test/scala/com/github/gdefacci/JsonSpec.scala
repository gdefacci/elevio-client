package com.github.gdefacci

import io.circe
import org.specs2._
import cats.effect.IO
import com.github.gdefacci.service.JsonModel

class JsonSpec extends mutable.Specification {

  def readJsonFile[T : circe.Decoder](path:String) = (
    for {
      cont <- TestUtil.classpathRead(path)
      v <- IO.fromEither(circe.parser.decode[T]( cont ))
    } yield v
  ).unsafeRunSync()

  def `article.json` = readJsonFile[JsonModel.Article]("article.json")
  def `articles1.json` = readJsonFile[JsonModel.Articles]("articles1.json")
  def `articles-list.json` = readJsonFile[JsonModel.Articles]("articles-list.json")
  
  "Json Specification" >> {

    "article.json" should {
      "have id == 41" in {
        `article.json`.id must beEqualTo(41)
      }
      "have list property keywords with size 3" in {
        `article.json`.keywords.length must beEqualTo(3)
      }
    }

    "articles-list.json" should {
      "have 2 articles" in {
        `articles-list.json`.articles.length must beEqualTo(2)
      }
      "have page_size with value 100" in {
        `articles-list.json`.pageSize must beEqualTo(100)
      }
      "have 1 total_pages" in {
        `articles-list.json`.totalPages must beEqualTo(1)
      }
      "have 2 total_entries" in {
        `articles-list.json`.totalEntries must beEqualTo(2)
      }
    }

    "articles1.json" should {
      "have 3 articles" in {
        `articles1.json`.articles.length must beEqualTo(3)
      }
    }

  }

}