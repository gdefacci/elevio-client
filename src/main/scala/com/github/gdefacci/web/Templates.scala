package com.github.gdefacci.web
import java.time.format.DateTimeFormatter

import com.github.gdefacci.service.{JsonModel, Page}

class Templates(appUrls:AppUrls) {

  def main(title:String, content:xml.Elem):xml.Elem =
    <html>
      <link href="/css/main.css" rel="stylesheet" />
      <meta charset="UTF-8" />
      <meta name="description" content="Elevio articles client" />
      <meta name="author" content="Giovanni De Facci" />
      <title>{title}</title>
    <body>
        {content}
    </body>
    </html>

  def articleItem(art:JsonModel.Article) = 
    <tr class="article-item">
      <td><a href={appUrls.getArticleById(art.id).renderString}>{art.title.getOrElse("no-title")}</a></td>
      <td>{art.author.map(_.name).getOrElse("no-author")}</td>
      <td>{art.status}</td>
      <td>{art.keywords.map(keyword)}</td>
      <td>{DateTimeFormatter.ISO_DATE_TIME.format(art.updatedAt)}</td>
    </tr>

  def articles(arts:Page[JsonModel.Article], navigation:xml.Elem) =
    <section class="articles-section">
      {articlesPageInfo(arts.pageNumber, arts.total)}
      <table class="articles">
        <tr><th>title</th><th>author</th><th>status</th><th>keywords</th><th>last updated</th></tr>
        {arts.items.map(articleItem)}
      </table>
      {navigation}
    </section>

  def homeLink =
    <div>
      <a class="home-link" href={appUrls.home.renderString}>Home</a>
    </div>

  def errorPage(err:String) =
    <div class="error-page">
      An error has happened : {err}
      <div class="navigation">
        {homeLink}
        {backLink}
      </div>
    </div>

  def backLink =
    <div>
      <a class="back" href="#" onClick="window.history.go(-1); return false;">Back</a>
    </div>

  def articlesNavigation(page:Page[JsonModel.Article]) =
    <div class="navigation">
      {if (page.pageNumber > 1)
        <div>
          <a class="prev-page" href={appUrls.getArticles(page.pageNumber - 1).renderString}>Prev</a>
        </div>
       else
          xml.NodeSeq.Empty
      }
      {homeLink}
      {if (page.pageNumber < page.total)
        <div>
          <a class="next-page" href={appUrls.getArticles(page.pageNumber + 1).renderString}>Next</a>
        </div>
      else
        xml.NodeSeq.Empty
      }
    </div>  

    def keywordSearchNavigation(keyword:String, page:Page[JsonModel.Article]) =
      <div class="navigation">
        {if (page.pageNumber > 1) 
          <div>
          <a class="prev-page" href={appUrls.keywordSearch(keyword, page.pageNumber - 1).renderString}>Prev</a>
          </div>
        else
          xml.NodeSeq.Empty
        }
        {homeLink}
        {if (page.pageNumber < page.total) 
          <div>
            <a class="next-page" href={appUrls.keywordSearch(keyword, page.pageNumber + 1).renderString}>Next</a>
          </div>
        else
          xml.NodeSeq.Empty
        }
        
      </div>    

  def keyword(k:String) = <span class="keyword">{k}</span>  

  def articlesPageInfo(curr:Int, total:Int) = 
    <div>Page <span class="page-number">{curr}</span> of <span class="page-total">{total}</span></div>

  def articleDetails(art:JsonModel.Article) = 
    <section class="article">
      <div><label>title</label>{art.title.getOrElse("no-title")}</div>
      <div><label>author</label>{art.author.map(_.name).getOrElse("no-author")}</div>
      <div><label>source</label>{art.source}</div>
      <div><label>notes</label>{art.notes.map(note).getOrElse("")}</div>
      <div><label>status</label>{art.status}</div>
      <div><label>keywords</label>{art.keywords.map(keyword)}</div>
      <div><label>last updated</label>{DateTimeFormatter.BASIC_ISO_DATE.format(art.updatedAt)}</div>
      <div>
        <label>smart groups</label>{ art.smartGroups.map(_.name).collect({ case Some(t) => smartGroup(t) }) }
      </div>
      <div>
        <h4>translations</h4>
        {art.translations.map(translation)}
      </div>
      <div class="navigation">
        {homeLink}
        {backLink}
      </div>
    </section>  

  def translation(t:JsonModel.Translation) =
    <div class="translation">
      <div><label>language</label>{t.languageId}</div>
      <div><label>title</label>{t.title.getOrElse("no-title")}</div>
      <div><label>summary</label>{t.summary.getOrElse("no-summary")}</div>
      <div class="content">{t.body.getOrElse("no-content")}</div>
    </div>

  def note(n:String) =
    <span class="note">{n}</span>

  def smartGroup(sg:String) = 
    <span class="smart-group">{sg}</span>

  def homepageContent(errors:HomePageErrors = HomePageErrors.empty) =
    <div>
      <div class="get-articles">
        <a href={appUrls.getArticles(1).renderString}>Available articles</a>
      </div>
      <div class="search-by-keyword">
        <form action={appUrls.submitKeywordSearch.renderString}>
          <input type="text" name="keyword"></input>
          {errors.keyword match {
          case None => xml.NodeSeq.Empty
          case Some(errText) =>
            <div class="field-error">{errText}</div>
          }}
          <input type="submit" value="Search"></input>
        </form>
      </div>
    </div>  
}

case class HomePageErrors(keyword:Option[String])

object HomePageErrors {

  val empty = HomePageErrors(None)

}