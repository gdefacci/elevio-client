Requires:
=========

- java, version >= 1.8 
- sbt, version >= 1.0

Launch
======

The command

```
sbt "runMain com.github.gdefacci.web.Main --key my-api-key --token my-token --port 8080"
```

starts the an http server on port 8080. 
The application will be available at:

http://localhost:8080
