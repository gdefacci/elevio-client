package com.github.gdefacci

import cats.effect.IO
import com.github.gdefacci.service.{ElevioArticlesRepository, ElevioArticlesUrls, JsonModel, Page}
import org.specs2._
import org.specs2.matcher._

object ElevioArticlesRepositorySpec extends mutable.Specification with IOMatchers with ResultMatchers {

  val articles:Seq[JsonModel.Article] = {
    def setKeywords: ((JsonModel.Article, Int)) => JsonModel.Article = {
      case ((p, idx)) =>
        val kws = (idx % 3) match {
          case 0 => List("the-keyword")
          case _ => Nil
        }
        p.copy(keywords = kws)
    }
    SampleData.articles(28).zipWithIndex.map(setKeywords)
  }
  val pageSize = 8
  val numberOfPages = Page.numberOfPages(articles.length, pageSize)

  val keyword = "the-keyword"
  val numberOfArticlesWithKeyword = articles.filter(_.keywords.contains(keyword)).length
  val searchNumberOfPages = Page.numberOfPages(numberOfArticlesWithKeyword, pageSize)

  val articlesRepository =
    new ElevioArticlesRepository(new ElevioArticlesRepository.Configuration[IO](
      TestClient(articles),
      new ElevioArticlesUrls(None, pageSize),
      "key",
      "token"
    ))

  def getArticlesPage1:IO[Unit] = for {
    pg1 <- articlesRepository.getArticles(1)
    _ <- IO { assert(pg1.pageNumber == 1) }
    _ <- IO { assert(pg1.items.length == pageSize) }
    _ <- IO { assert(pg1.total == numberOfPages) }
  } yield ()

  def getArticlesPage2:IO[Unit] = for {
    pg1 <- articlesRepository.getArticles(2)
    _ <- IO { assert(pg1.pageNumber == 2) }
    _ <- IO { assert(pg1.items.length == pageSize) }
    _ <- IO { assert(pg1.total == numberOfPages) }
  } yield ()

  def getArticlesPageLast:IO[Unit] = for {
    pg1 <- articlesRepository.getArticles(4)
    _ <- IO { assert(pg1.pageNumber == 4) }
    _ <- IO { assert(pg1.items.length == 4) }
    _ <- IO { assert(pg1.total == numberOfPages) }
  } yield ()

  def searchArticlesPage1:IO[Unit] = for {
    pg1 <- articlesRepository.getArticlesByKeyword(keyword, 1)
    _ <- IO { assert(pg1.pageNumber == 1) }
    _ <- IO { assert(pg1.items.length == pageSize) }
    _ <- IO { assert(pg1.total == searchNumberOfPages, s"${pg1.total} != $searchNumberOfPages") }
  } yield ()

  def searchArticlesPageLast:IO[Unit] = for {
    pg1 <- articlesRepository.getArticlesByKeyword(keyword, 2)
    _ <- IO { assert(pg1.pageNumber == searchNumberOfPages) }
    _ <- IO { assert(pg1.items.length == 2) }
    _ <- IO { assert(pg1.total == searchNumberOfPages) }
  } yield ()

  def getArticlesBeforeFirstPage:IO[Unit] = for {
    pg1 <- articlesRepository.getArticles(page = 1)
    pg2 <- articlesRepository.getArticles(page = -2)
    _ <- IO { assert(pg1 == pg2) }
  } yield ()

  def getArticlesAfterLastPage:IO[Unit] = for {
    pg1 <- articlesRepository.getArticles(page = numberOfPages)
    pg2 <- articlesRepository.getArticles(page = 12)
    _ <- IO { assert(pg1 == pg2) }
  } yield ()

  def searchArticlesBeforeFirstPage:IO[Unit] = for {
    pg1 <- articlesRepository.getArticlesByKeyword(keyword, 1)
    pg2 <- articlesRepository.getArticlesByKeyword(keyword, -2)
    _ <- IO { assert(pg1 == pg2) }
  } yield ()

  def searchArticlesAfterLastPage:IO[Unit] = for {
    pg1 <- articlesRepository.getArticlesByKeyword(keyword, 2)
    pg2 <- articlesRepository.getArticlesByKeyword(keyword, 12)
    _ <- IO { assert(pg1 == pg2) }
  } yield ()

  def getExistingArticle:IO[Unit] = for {
    art <- articlesRepository.getArticleById(id = 7)
    _ <- IO { assert(art.source == "source 7") }
  } yield ()

  def getNonExistingArticle:IO[Unit] = for {
    _ <- articlesRepository.getArticleById(id = -12)
  } yield ()

  def fail[A]: Matcher[IO[A]]  = ((_: IO[A]).attempt.map {
    case Left(_) => true
    case Right(_) => false
  }.unsafeRunSync(), "expecting a failure")


  "Get Articles" >> {
    "page 1" >> {
      getArticlesPage1 must returnOk
    }
    "page 2" >> {
      getArticlesPage2 must returnOk
    }
    "last" >> {
      getArticlesPageLast must returnOk
    }
    "before first page" >> {
      getArticlesBeforeFirstPage must returnOk
    }
    "after last page" >> {
      getArticlesAfterLastPage must returnOk
    }
  }

  "Search Articles" >> {
    "page 1" >> {
      searchArticlesPage1 must returnOk
    }
    "last" >> {
      searchArticlesPageLast must returnOk
    }
    "before first page" >> {
      searchArticlesBeforeFirstPage must returnOk
    }
    "after last page" >> {
      searchArticlesAfterLastPage must returnOk
    }
  }

  "Get articles by id" >> {
    "existing article" >> {
      getExistingArticle must returnOk
    }
    "missing article" >> {
      getNonExistingArticle must fail
    }
  }

}
