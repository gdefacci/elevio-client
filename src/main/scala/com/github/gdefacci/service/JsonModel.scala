package com.github.gdefacci.service

import java.time.ZonedDateTime

import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}

object JsonModel {

  implicit val config: Configuration =
    Configuration.default.withSnakeCaseMemberNames

  @ConfiguredJsonCodec
  case class Article(
      id: Int,
      order: Int,
      title: Option[String],
      author: Option[Author],
      source: String,
      externalId: Option[String],
      editorVersion: String,
      notes: Option[String],
      keywords: List[String],
      categoryId: Int,
      access: String,
      accessEmails: List[String],
      accessDomains: List[String],
      accessGroups: Option[List[String]],
      smartGroups: List[SmartGroup],
      status: String,
      lastPublisher: Option[Author],
      lastPublishedAt: Option[ZonedDateTime],
      contributors: Option[List[Author]],
      createdAt: Option[ZonedDateTime],
      updatedAt: ZonedDateTime,
      translations: List[Translation]
  )

  @ConfiguredJsonCodec
  case class Translation(
      languageId: String,
      title: Option[String],
      body: Option[String],
      summary: Option[String],
      machineSummary: Option[String]
  )

  @ConfiguredJsonCodec
  case class Author(id: Int, name: String, gravatar: String, email: String)

  @ConfiguredJsonCodec
  case class SmartGroup(id: Int, name: Option[String])

  @ConfiguredJsonCodec
  case class Articles(
      articles: Seq[Article],
      pageNumber: Int,
      pageSize: Int,
      totalPages: Int,
      totalEntries: Int
  )

  @ConfiguredJsonCodec
  case class ArticleResult(article: Article)

}
